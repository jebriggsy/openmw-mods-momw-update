# Copyright Copyright 2019-2023 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from common.mw import MW, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Dunmer Lanterns Replacer"
    DESC = "Replaces the Dunmer lanterns from morrowind with smooth, detailed versions"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/43219"
    # Note: We handle this in an unintuitive manner, however the README states that
    # Derivatives are allowed under the conditions of attribution and non-commercial use
    LICENSE = "attribution-nc free-derivation"
    NEXUS_SRC_URI = "https://nexusmods.com/morrowind/mods/43219?tab=files&file_id=1000036288 -> Dunmer_Lanterns_Replacer-43219-13-2-1677973534.7z"
    KEYWORDS = "openmw"
    TIER = 1
    IUSE = "textures-pherim textures-swg minimal tr glow oaab"
    RDEPEND = """
        tr? ( landmasses/tamriel-rebuilt )
    """
    REQUIRED_USE = "?? ( textures-pherim textures-swg )"
    INSTALL_DIRS = [
        InstallDir("00 Core"),
        InstallDir("01 Glow Effect", REQUIRED_USE="glow"),
        InstallDir(
            "02 Ashlander Lantern (Smoothed Only)", REQUIRED_USE="minimal !glow"
        ),
        InstallDir(
            "02 Ashlander Lantern (Smoothed Only) - Glow Effect",
            REQUIRED_USE="minimal glow",
        ),
        InstallDir(
            "03 Ashlander Lanterns Retexture 1",
            REQUIRED_USE="textures-pherim",
        ),
        InstallDir(
            "03 Ashlander Lanterns Retexture 2",
            REQUIRED_USE="textures-swg",
        ),
        InstallDir("04 Tamriel_Data Patch", REQUIRED_USE="tr !glow"),
        InstallDir("04 Tamriel_Data Patch - Glow Effect", REQUIRED_USE="tr glow"),
        InstallDir(PATH="05 OAAB_Data Patch", REQUIRED_USE="oaab"),
        InstallDir(PATH="05 OAAB_Data Patch - Glow Effect", REQUIRED_USE="oaab glow"),
    ]
