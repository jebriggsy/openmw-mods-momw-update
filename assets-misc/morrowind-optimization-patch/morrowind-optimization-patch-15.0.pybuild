# Copyright Copyright 2019-2022 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from common.mw import MW, File, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Morrowind Optimization Patch"
    DESC = "Greatly improves Morrowind performance and fixes some mesh errors."
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/45384"
    NEXUS_URL = HOMEPAGE
    LICENSE = "|| ( free-distribution free-derivation )"
    RDEPEND = """
        fjalding-fix? ( base/morrowind[tribunal,bloodmoon] )
        weapon-sheathing? ( gameplay-weapons/weapon-sheathing )
        !weapon-sheathing? ( !!gameplay-weapons/weapon-sheathing )
        better-bodies? ( npcs-bodies/better-bodies )
        !better-bodies? ( !!npcs-bodies/better-bodies )
    """
    DATA_OVERRIDES = "assets-meshes/correct-uv-rocks"
    KEYWORDS = "openmw"
    NEXUS_SRC_URI = "https://nexusmods.com/morrowind/mods/45384?tab=files&file_id=1000031159 -> Morrowind_Optimization_Patch-45384-1-15-0-1653821581.7z"
    IUSE = "+fjalding-fix weapon-sheathing vanilla-textures better-bodies"
    TEXTURE_SIZES = "362"
    TIER = 1
    INSTALL_DIRS = [
        InstallDir("00 Core"),
        InstallDir(
            "01 Lake Fjalding Anti-Suck",
            PLUGINS=[File("Lake Fjalding Anti-Suck.ESP")],
            REQUIRED_USE="fjalding-fix",
        ),
        InstallDir(
            "02 Weapon Sheathing Patch",
            REQUIRED_USE="weapon-sheathing",
            PATCHDIR="weapon-sheathing-patch",
            DATA_OVERRIDES="gameplay-weapons/weapon-sheathing",
        ),
        InstallDir(
            "03 Chuzei Fix",
            PLUGINS=[File("chuzei_helm_no_neck.esp")],
            REQUIRED_USE="!better-bodies",
        ),
        InstallDir("04 Better Vanilla Textures", REQUIRED_USE="vanilla-textures"),
    ]
