# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from common.mw import MW, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Landscape Retexture"
    DESC = "Retexture of all regions "
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/42575"
    KEYWORDS = "openmw"
    LICENSE = "all-rights-reserved"
    TEXTURE_SIZES = "1024 2048"
    IUSE = "map_normal bloodmoon"
    NEXUS_SRC_URI = """
        texture_size_1024? (
            https://www.nexusmods.com/morrowind/mods/42575?tab=files&file_id=1000009042
            -> Landscape_Retexture_1K-42575-2-5.rar
        )
        texture_size_2048? (
            https://www.nexusmods.com/morrowind/mods/42575?tab=files&file_id=1000009041
            -> Landscape_Retexture_2K-42575-2-5.rar
        )
        map_normal? (
            https://www.nexusmods.com/morrowind/mods/42575?tab=files&file_id=1000015021
            -> Landscape_retexture_-_OpenMW_Normal_map_and_Parallax_patch-42575-1-0-1561924428.rar
        )
        bloodmoon? (
            https://www.nexusmods.com/morrowind/mods/42575?tab=files&file_id=1000000576
            -> Bloodmoon_Landscape_Retexture-42575-1-0.rar
        )
    """
    DATA_OVERRIDES = "assets-textures/tyddys-landscape-retexture"
    INSTALL_DIRS = [
        InstallDir(
            ".",
            S="Landscape_Retexture_1K-42575-2-5",
            REQUIRED_USE="texture_size_1024",
        ),
        InstallDir(
            ".",
            S="Landscape_Retexture_2K-42575-2-5",
            REQUIRED_USE="texture_size_2048",
        ),
        InstallDir(
            "Landscape retexture Normal map & parallax",
            S="Landscape_retexture_-_OpenMW_Normal_map_and_Parallax_patch-42575-1-0-1561924428",
            REQUIRED_USE="map_normal",
        ),
        InstallDir(
            ".",
            S="Bloodmoon_Landscape_Retexture-42575-1-0",
            REQUIRED_USE="bloodmoon",
        ),
    ]
