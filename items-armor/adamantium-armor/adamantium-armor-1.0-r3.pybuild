# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os

from common.mw import MW, File, InstallDir, apply_patch


class Package(MW):
    NAME = "Adamantium Armor"
    DESC = "Adds the titular armor from the Tribunal expansion to various locations"
    HOMEPAGE = """
        https://elderscrolls.bethesda.net/en/morrowind
        https://gitlab.com/bmwinger/umopp
    """
    # Original is all-rights-reserved
    # UMOPP is attribution
    LICENSE = "all-rights-reserved attribution"
    RESTRICT = "mirror"
    RDEPEND = "base/morrowind[tribunal?]"
    DEPEND = ">=bin/delta-plugin-0.19"
    KEYWORDS = "openmw"
    IUSE = "tribunal"
    SRC_URI = """
        https://cdn.bethsoft.com/elderscrolls/morrowind/other/adamantiumarmor.zip
        https://gitlab.com/bmwinger/umopp/uploads/daccf7d8273e9605277e42151ba89935/adamantiumarmor-umopp-3.2.0.2.tar.xz
    """
    INSTALL_DIRS = [
        InstallDir(".", PLUGINS=[File("adamantiumarmor.esp")], S="adamantiumarmor")
    ]

    def src_prepare(self):
        # From instructions in README.md
        path = os.path.join(self.WORKDIR, "adamantiumarmor-umopp-3.2.0.2")
        if "tribunal" in self.IUSE:
            self.execute(
                "delta_plugin apply "
                + os.path.join(path, "adamantiumarmor-compat-patch.yaml")
            )
        else:
            self.execute(
                "delta_plugin apply " + os.path.join(path, "adamantiumarmor-patch.yaml")
            )

        patches = [
            "Boots_A",
            "Boots_F",
            "Boots_GND",
            "Bracer_GND",
            "Bracer_W",
            "CL_Pauldron",
            "Greaves_G",
            "Greaves_K",
            "Greaves_UL",
            "Pauldron_FA",
            "Pauldron_UA",
        ]

        for patch in patches:
            apply_patch(os.path.join(path, f"{patch}.patch"))
