# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from common.mw import MW, File, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Improved Better Skulls"
    DESC = 'Improved version of "Better Skulls" by tronvillain & nONatee.'
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/46012"
    LICENSE = "all-rights-reserved"  # Derived from bethesda's assets
    RDEPEND = "base/morrowind[bloodmoon]"
    KEYWORDS = "openmw"
    SRC_URI = """
        Skeletons_Atlased-46012-1-2-1583908991.7z
        Improved_Better_Skulls-46012-2-6-1566053352.7z
    """
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/46012"
    TEXTURE_SIZES = """
        atlas? (
            vanilla-textures? ( 512 1024 )
            darknut-textures? ( 1024 2048 )
            connary-textures? ( 1448 )
        )
        !atlas? (
            vanilla-textures? ( 32 )
            connary-textures? ( 724 )
        )
    """
    IUSE = "+atlas vanilla-textures darknut-textures +connary-textures"
    REQUIRED_USE = """
        ^^ ( vanilla-textures darknut-textures connary-textures )
        !atlas? ( !darknut-textures )
    """
    INSTALL_DIRS = [
        InstallDir(
            "Data Files",
            PLUGINS=[File("Better Skulls.ESP")],
            S="Improved_Better_Skulls-46012-2-6-1566053352",
            REQUIRED_USE="connary-textures",
        ),
        InstallDir(
            "Vanilla Textures",
            S="Improved_Better_Skulls-46012-2-6-1566053352",
            REQUIRED_USE="vanilla-textures",
        ),
        InstallDir("Fixed Skeletons", S="Improved_Better_Skulls-46012-2-6-1566053352"),
        InstallDir(
            "03 Darknut's Textures 512",
            S="Skeletons_Atlased-46012-1-2-1583908991",
            REQUIRED_USE="atlas darknut-textures texture_size_1024",
        ),
        InstallDir(
            "03a Darknut's Textures 1024",
            S="Skeletons_Atlased-46012-1-2-1583908991",
            REQUIRED_USE="atlas darknut-textures texture_size_2048",
        ),
        InstallDir(
            "02a Vanilla Textures 2x-4x Texture Size",
            S="Skeletons_Atlased-46012-1-2-1583908991",
            REQUIRED_USE="atlas vanilla-textures texture_size_1024",
        ),
        InstallDir(
            "01 Improved Better Skulls Textures (Connary)",
            S="Skeletons_Atlased-46012-1-2-1583908991",
            REQUIRED_USE="atlas connary-textures",
        ),
        InstallDir(
            "02 Vanilla Textures",
            S="Skeletons_Atlased-46012-1-2-1583908991",
            REQUIRED_USE="atlas vanilla-textures texture_size_512",
        ),
    ]
