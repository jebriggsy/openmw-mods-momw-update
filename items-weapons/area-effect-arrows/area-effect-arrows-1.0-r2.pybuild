# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os

from common.mw import MW, File, InstallDir


class Package(MW):
    NAME = "Area Effect Arrows"
    DESC = "Adds a new shop selling a wide selection of quality Marksman weapons"
    HOMEPAGE = """
        https://elderscrolls.bethesda.net/en/morrowind
        https://gitlab.com/bmwinger/umopp
    """
    # Original is all-rights-reserved
    # UMOPP is attribution
    LICENSE = "all-rights-reserved attribution"
    RESTRICT = "mirror"
    RDEPEND = "base/morrowind"
    DEPEND = ">=bin/delta-plugin-0.15 <bin/delta-plugin-0.19"
    KEYWORDS = "openmw"
    SRC_URI = """
        https://cdn.bethsoft.com/elderscrolls/morrowind/other/area_effect_arrows.zip
        https://gitlab.com/bmwinger/umopp/uploads/8cedc2f50759535fe2dff8fc8c1c1e1e/areaeffectarrows-umopp-3.2.0.tar.xz
    """
    INSTALL_DIRS = [
        InstallDir(".", PLUGINS=[File("AreaEffectArrows.esp")], S="area_effect_arrows")
    ]

    def src_prepare(self):
        # From instructions in README.md
        path = os.path.join(self.WORKDIR, "areaeffectarrows-umopp-3.2.0")
        self.execute(
            "delta_plugin -v apply " + os.path.join(path, "AreaEffectArrows.patch")
        )
