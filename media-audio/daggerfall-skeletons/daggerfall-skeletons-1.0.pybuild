# Copyright 2023 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from common.mw import MW, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Daggerfall Skeleton Sounds"
    DESC = " Adds back the Daggerfall skeleton roars straight from Hell"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/45838"
    # The mod author says we can re-upload it, but it's distributing a Bethesda asset,
    # so we probably shouldn't.
    LICENSE = "all-rights-reserved"
    RDEPEND = "base/morrowind"
    KEYWORDS = "openmw"
    # Better Sounds changes the sound paths, making this mod do nothing.
    RDEPEND = """
        !!media-audio/better-sounds
    """
    NEXUS_SRC_URI = """
        https://www.nexusmods.com/morrowind/mods/45838?tab=files&file_id=1000010954
        -> Daggerfall_Skeleton_Sounds-45838-1-0.7z
    """
    INSTALL_DIRS = [InstallDir("Data Files")]
