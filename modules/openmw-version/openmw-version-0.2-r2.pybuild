# Copyright 2019-2023 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
import shutil
import sys

from pybuild import Pybuild2


class Package(Pybuild2):
    NAME = "OpenMW Version detection script"
    DESC = (
        "Adds a .get_arch_version.py file for passing the version of OpenMW to Portmod"
    )
    LICENSE = "GPL-3"
    KEYWORDS = "~openmw"

    def src_prepare(self):
        print("Testing version script.")
        print(sys.executable, os.path.join(self.FILESDIR, ".get_arch_version.py"))
        print("OpenMW version is: ", end="")
        self.execute(
            [sys.executable, os.path.join(self.FILESDIR, ".get_arch_version.py")],
            check=True,
        )

    def src_install(self):
        shutil.copy(
            os.path.join(self.FILESDIR, ".get_arch_version.py"),
            os.path.join(self.D, ".get_arch_version.py"),
        )
