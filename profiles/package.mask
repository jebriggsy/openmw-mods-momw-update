# When masking packages for removal, include the following information
#
# 1) Name, email/gitlab username and date of entry
# 2) Reason for masking
# 3) Link to the issue for the removal
# 4) Date of removal
#
# Once the given date has passed, the package and the entry in this file can be removed
# Make sure to also remove references such as dependencies, optional or otherwise


# Version no longer available upstream
=items-misc/wares-2.2.2-r1

# PopeRigby <poperigby@mailbox.org> (2023-03-21)
# Hidden on Nexus Mods
# Removal on 2023-05-01. Issue https://gitlab.com/portmod/openmw-mods/-/issues/350
npcs-bodies/mackoms-khajiit

# This is a placeholder package, it cannot be installed! Consider contributing! https://portmod.gitlab.io/portmod/dev/index.html
common/placeholder
